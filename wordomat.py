__author__ = 'Baumfaust'

import sys
import random
import csv
from enum import Enum
from time import clock, strftime

from PySide.QtGui import QApplication, QWidget
from PySide import QtCore

from mainwindow import Ui_MainWindow


TUT_LEN = 2
DEL_TUT_WORDS = False


class Category(Enum):
    Positive = 1
    Negative = 2


class Assignment(object):
    def __init__(self, word="", given=0, selected=0, time=0):
        self.word = word
        self.given_cat = given
        self.selected_cat = selected
        self.time = time

    def __repr__(self):
        return "Word: %s, Given: %s, Selected: %s, Time: %s" % (self.word, self.given_cat.name, self.selected_cat.name,
                                                                self.time)


class WordOMat(QWidget, Ui_MainWindow):
    def __init__(self):
        super(WordOMat, self).__init__()
        self.setupUi(self)
        self.mainpage.setCurrentIndex(0)
        self.test_started = False
        self.installEventFilter(self)
        self.word_list = []
        self.word_list_iter = iter(self.word_list)
        self.tut_list = []
        self.assignments = []
        self.curr_assign = Assignment()
        self.start_time = 0

    def tutorial(self, words):
        for i in range(TUT_LEN):
            word = random.choice(words)
            words.remove(word)
            self.tut_list.append((word, Category.Positive))
            self.tut_list.append((word, Category.Negative))

    def import_data(self):
        self.word_list.clear()
        self.assignments.clear()

        with open("words.txt", encoding="utf-8") as f:
            words = [line.rstrip('\n') for line in f]

        self.tutorial(words)

        for word in words:
            self.word_list.append((word, Category.Positive))
            self.word_list.append((word, Category.Negative))

        random.shuffle(self.word_list)
        self.tut_list.extend(self.word_list)
        self.word_list = self.tut_list
        self.word_list_iter = iter(self.word_list)

    def next_word(self):
        try:
            ele = next(self.word_list_iter)
            self.curr_assign = Assignment(ele[0], ele[1])
            self.label_word.setText(self.curr_assign.word)
            if self.curr_assign.given_cat == Category.Positive:
                self.label_word.setStyleSheet("QLabel { color : green; }")
            else:
                self.label_word.setStyleSheet("QLabel { color : red; }")
            self.start_time = clock()

        except StopIteration:
            self.end_test()

    def export_data(self):
        time_str = strftime("%Y%m%d-%H%M%S")

        with open("results-" + time_str + ".csv", 'w', newline='') as results:
            writer = csv.writer(results, delimiter=';')
            start = 0
            if DEL_TUT_WORDS:
                start = TUT_LEN * 2
            for assign in self.assignments[start:]:
                writer.writerow(
                    [assign.word, assign.given_cat.name, assign.selected_cat.name, str(round(assign.time * 1000))])

    def end_test(self):
        self.test_started = False
        self.mainpage.setCurrentIndex(2)
        self.export_data()

    def start_test(self):
        self.import_data()
        self.mainpage.setCurrentIndex(1)
        self.next_word()
        self.test_started = True

    def handle_selection(self, cat):
        t2 = clock()
        self.curr_assign.selected_cat = cat
        self.curr_assign.time = t2 - self.start_time
        self.assignments.append(self.curr_assign)
        self.next_word()

    def eventFilter(self, widget, event):
        if event.type() == QtCore.QEvent.KeyPress:
            key = event.key()
            if self.test_started:
                if key == QtCore.Qt.Key_Right:
                    self.handle_selection(Category.Negative)
                    return True
                if key == QtCore.Qt.Key_Left:
                    self.handle_selection(Category.Positive)
                    return True
            else:
                if key == QtCore.Qt.Key_Space:
                    self.start_test()
                    return True
        return QWidget.eventFilter(self, widget, event)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    main_app = WordOMat()
    main_app.setWindowState(QtCore.Qt.WindowFullScreen)
    main_app.show()
    ret = app.exec_()
    sys.exit(ret)